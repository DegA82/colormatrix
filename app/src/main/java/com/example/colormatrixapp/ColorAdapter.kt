package com.example.colormatrixapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrixapp.databinding.ItemColorBinding

class ColorAdapter : RecyclerView.Adapter<ColorAdapter.ColorViewHolder>() {

    //new code
//    private lateinit var mListener: onItemClickListener
//    interface onItemClickListener(
//        fun onItemClick(position: Int)
//    )
//    fun setOnItemClickListener(listener: onItemClickListener){
//        mListener = listener
//    }
// above is new code
    private lateinit var colors: IntArray

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val binding = ItemColorBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ColorViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val color = colors[position]
        holder.loadColor(color)
    }

    override fun getItemCount(): Int {
        return colors.size
    }

    fun addColors(colors: IntArray) {
        this.colors = colors
        notifyDataSetChanged()
    }

    class ColorViewHolder(
        private val binding: ItemColorBinding  //new code listener: onItemClickListener
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(color: Int) {
            binding.ivColor.setBackgroundColor(color)
        }
//        init {
//            itemView.setOnClickListener {
//                listener.onItemClick()
//            }
        }

}

//new code
//    private lateinit var mListener: onItemClickedListener
//    interface onItemClickedListener(
//        fun onItemClick(position: Int)
//    )
//    fun setOnClickListener(listener: onItemClickedListener){
//        mListener = listener
//    }
