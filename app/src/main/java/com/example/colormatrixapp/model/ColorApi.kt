package com.example.colormatrixapp.model

interface ColorApi {
    suspend fun colorDetail(count:Int): IntArray
}