package com.example.colormatrixapp.model

import com.example.colormatrixapp.RandomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ColorRepo {
    private val colorApi = object : ColorApi {
        override suspend fun colorDetail(count: Int): IntArray {
            return IntArray(count) {
                RandomColor.randomColor
            }
        }
    }

    suspend fun colorDetail(count: Int): IntArray = withContext(Dispatchers.IO) {
        colorApi.colorDetail(count)
    }
}