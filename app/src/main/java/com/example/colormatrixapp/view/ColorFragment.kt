package com.example.colormatrixapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.colormatrixapp.ColorAdapter
import com.example.colormatrixapp.databinding.FragmentColorBinding
import com.example.colormatrixapp.viewmodel.ColorViewModel


class ColorFragment: Fragment() {

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvColorList.layoutManager = GridLayoutManager(context, 3)
        colorViewModel.color.observe(viewLifecycleOwner){
        binding.rvColorList.adapter = ColorAdapter().apply {addColors(it) }
        }
        binding.btnColor.setOnClickListener{
            colorViewModel.getRandomColor(binding.etText.text.toString().toInt())
            }
//        private fun colorClicked(color:Int){
//            Toast.makeText(this,color, Toast.LENGTH_LONG).show()
//        }
//        val action =
//            ColorFragmentDirections.actionColorFragmentToDisplayFragment()
//        findNavController().navigate(action)

//        val adapter = ColorAdapter()
//        binding.adapter = adapter
//        adapter.setOnItemClickListener(object: ColorAdapter.onItemClickListener{
//            override fun onItemClick(position: Int){
//
//            }
//        })

    }
}