package com.example.colormatrixapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colormatrixapp.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel:ViewModel() {
    private val colorRepo = ColorRepo
    private var _color = MutableLiveData<IntArray>()
    val color:LiveData<IntArray> get() = _color

    fun getRandomColor(count:Int){
        viewModelScope.launch {
            _color.value = colorRepo.colorDetail(count)
        }
    }
}